# Upgrade Amq Streams

test 

## Tareas principales,
- Subir imágenes en registry de openshift.
- Detener replicación de golden gate hacia Kafka
- Actualizar amq-streams
- Actualizar librería de kafka utilizada por golden gate.

***
## [Instalación 1.4.1](amq-streams-1.4.1)

- Hacer backup
```bash
oc get all -l app=strimzi -o yaml > backup/strimzi-backup.yaml
```


- Aplicar cambio
```bash
oc apply -f install/cluster-operator -n amq-streams
```

- Esperar redespliegue de pods

- Modificar recurso Kafka con nueva versión
```bash
oc edit dc/kafka
```

```yaml
log.message.format.version: "2.4"
version: 2.4.0
```

- Esperar redespliegue de pods

- Verificar broker de Kafka actualizados con nueva versión

```bash
$ oc get statefulset.apps -o wide
NAME                   DESIRED   CURRENT   AGE       CONTAINERS          IMAGES
production-kafka       3         3         1y        kafka,tls-sidecar   docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-24-rhel7:1.4.1,docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-24-rhel7:1.4.1
production-zookeeper   3         3         2y        zookeeper           docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-24-rhel7:1.4.1,docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-24-rhel7:1.4.1
```

- Activar replicación de golden gate hacia Kafka
- Actualizar librería



***


## [Instalación 1.5.1](amq-streams-1.5.1)



- Hacer backup
```bash
oc get all -l app=strimzi -o yaml > backup/strimzi-backup.yaml
```


- Aplicar cambio
```bash
oc apply -f install/cluster-operator -n amq-streams
```

- Esperar redespliegue de pods

- Modificar recurso Kafka con nueva versión
```bash
oc edit dc/kafka
```

```yaml
log.message.format.version: "2.5"
version: 2.5.0
```

- Esperar redespliegue de pods

- Verificar broker de Kafka actualizados con nueva versión

```bash
$ oc get statefulset.apps -o wide
NAME                   DESIRED   CURRENT   AGE       CONTAINERS          IMAGES
production-kafka       3         3         1y        kafka,tls-sidecar   docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-25-rhel7:1.5.0,docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-25-rhel7:1.5.0
production-zookeeper   3         3         2y        zookeeper           docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-25-rhel7:1.5.0
```

Fuente: [Guia de Instalación](https://access.redhat.com/documentation/en-us/red_hat_amq/7.7/html-single/deploying_and_upgrading_amq_streams_on_openshift/index#assembly-upgrade-str)


## [Instalación 1.6.2](amq-streams-1.6.2)



- Hacer backup
```bash
oc get all -l app=strimzi -o yaml > backup/strimzi-backup.yaml
```


- Aplicar cambio
```bash
oc apply -f install/cluster-operator -n amq-streams
```

- Esperar redespliegue de pods

- Modificar recurso Kafka con nueva versión
```bash
oc edit dc/kafka
```

```yaml
log.message.format.version: "2.6"
version: 2.6.0
```

- Esperar redespliegue de pods

- Verificar broker de Kafka actualizados con nueva versión

```bash
$ oc get statefulset.apps -o wide
NAME                   DESIRED   CURRENT   AGE       CONTAINERS   IMAGES
production-kafka       3         3         1y        kafka        docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-26-rhel7:1.6.2
production-zookeeper   3         3         2y        zookeeper    docker-registry.default.svc:5000/amq-streams/amq-streams-kafka-26-rhel7:1.6.2
```

Fuente: [Guia de Instalación](https://access.redhat.com/documentation/en-us/red_hat_amq/2020.Q4/html-single/using_amq_streams_on_openshift/index#assembly-upgrade-cluster-operator-str)


